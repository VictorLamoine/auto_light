#include <cstddef>
#include <FreeRTOS.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rcc.h>
#include <task.h>
#include "usart.hpp"

#define GPIO_PORT_LED GPIOC
#define GPIO_LED GPIO13

#define GPIO_MOVEMENT_SENSOR GPIO0
#define GPIO_LED_STRIP GPIO1
#define GPIO_PHOTORESISTOR GPIO2

#define PHOTO_RESISTOR_THREHSOLD 1000
#define SWITCH_OFF_DELAY 30 // seconds

void exti0_isr(void)
{
  exti_reset_request(EXTI0);
}

void low_power_stop()
{
  // Configure sleep + go to sleep
  SCB_SCR |= SCB_SCR_SLEEPDEEP; // Set deepsleep bit
  pwr_set_stop_mode(); // Stop mode
  PWR_CR |= PWR_CR_LPDS; // Voltage regulator off
  exti_reset_request(EXTI0);
  __asm volatile("wfi"); // Sleep and wait for interrupt (rising edge) on PA0
  // HSI clock is used after stop mode
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  SCB_SCR &= ~SCB_SCR_SLEEPDEEP; // Clear deepsleep bit
}

uint16_t read_adc()
{
  adc_start_conversion_direct(ADC1);
  while (!adc_eoc(ADC1)) {}
  return adc_read_regular(ADC1);
}

void task_led_strip(void *)
{
  uint32_t count(0);
  uint32_t tick_start(xTaskGetTickCount());
  bool too_bright(false);

  while (1)
  {
    ++count;
    uint32_t seconds_elapsed((xTaskGetTickCount() - tick_start) / 1000); // "ok" with overflow

    const uint16_t photo_resistor(read_adc());
    usart_puts(std::to_string(count) + " | photo resistor = " + std::to_string(photo_resistor));
    if (photo_resistor > PHOTO_RESISTOR_THREHSOLD)
    {
      usart_puts(std::to_string(count) + " | too bright!");
      too_bright = true;
      seconds_elapsed = -1;
    }
    else if (gpio_get(GPIOA, GPIO_MOVEMENT_SENSOR))
    {
      gpio_set(GPIOA, GPIO_LED_STRIP);
      usart_puts(std::to_string(count) + " | movement");
      vTaskDelay(pdMS_TO_TICKS(500));
      tick_start = xTaskGetTickCount();
      continue;
    }

    usart_puts(std::to_string(count) + " | seconds_elapsed = " + std::to_string(seconds_elapsed));
    if (seconds_elapsed < SWITCH_OFF_DELAY)
    {
      vTaskDelay(pdMS_TO_TICKS(500));
      continue;
    }

    usart_puts(std::to_string(count) + " | disable light and sleep");
    vTaskDelay(pdMS_TO_TICKS(100)); // Leave some time to send the USART messages
    gpio_clear(GPIOA, GPIO_LED_STRIP);
    gpio_set(GPIO_PORT_LED, GPIO_LED); // LED off
    too_bright = false;

    low_power_stop();
    // Woke up because movement
    vTaskDelay(pdMS_TO_TICKS(500));
    tick_start = xTaskGetTickCount();
  }
}

void task_blink(void *)
{
  while (1)
  {
    if (gpio_get(GPIOA, GPIO_MOVEMENT_SENSOR))
      gpio_clear(GPIO_PORT_LED, GPIO_LED); // LED on
    else
      gpio_set(GPIO_PORT_LED, GPIO_LED); // LED off
    vTaskDelay(pdMS_TO_TICKS(100));
  }
}

int main(void)
{
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  usart_setup();
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIO_PORT_LED,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO_LED);
  gpio_set(GPIO_PORT_LED, GPIO_LED); // Turn off

  rcc_periph_clock_enable(RCC_GPIOA);
  // Presence sensor
  gpio_set_mode(GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                GPIO_MOVEMENT_SENSOR);
  gpio_clear(GPIOA, GPIO_MOVEMENT_SENSOR);

  // Transistor base to control LED strip
  gpio_set_mode(GPIOA,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO_LED_STRIP);
  gpio_clear(GPIOA, GPIO_LED_STRIP); // Disable LED strip

  gpio_set_mode(GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                GPIO_PHOTORESISTOR);

  // Configure ADC
  const uint8_t adc_channel(ADC_CHANNEL2); // PA2
  rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_ADC1EN);
  adc_power_off(ADC1);
  rcc_peripheral_reset(&RCC_APB2RSTR, RCC_APB2RSTR_ADC1RST);
  rcc_peripheral_clear_reset(&RCC_APB2RSTR, RCC_APB2RSTR_ADC1RST);
  adc_set_dual_mode(ADC_CR1_DUALMOD_IND);
  rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV6);
  adc_disable_scan_mode(ADC1);
  adc_set_right_aligned(ADC1);
  adc_set_single_conversion_mode(ADC1);
  uint8_t channel_array[] = { adc_channel };
  adc_set_regular_sequence(ADC1, 1, channel_array);
  adc_set_sample_time(ADC1, adc_channel, ADC_SMPR_SMP_239DOT5CYC);
  adc_power_on(ADC1);
  adc_reset_calibration(ADC1);
  adc_calibrate_async(ADC1);
  while (adc_is_calibrating(ADC1));

  // Configure wake up
  rcc_periph_clock_enable(RCC_AFIO); // EXTI
  exti_select_source(EXTI0, GPIOA); // PA0
  EXTI_IMR = 0x1; // Configure interrupt mask register for PA0
  exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
  exti_enable_request(EXTI0);
  nvic_enable_irq(NVIC_EXTI0_IRQ); // PC0 interrupt

  xTaskCreate(task_led_strip, "led_strip", 400, NULL, configMAX_PRIORITIES - 1, NULL);
  xTaskCreate(task_blink, "blink", 300, NULL, configMAX_PRIORITIES - 1, NULL);
  xTaskCreate(task_usart_tx, "usart", 100, NULL, configMAX_PRIORITIES - 2, NULL);
  vTaskStartScheduler();
  while (1);
  return 0;
}
