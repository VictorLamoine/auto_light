#ifndef USART_HPP
#define USART_HPP

#include <FreeRTOS.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/usart.h>
#include <queue.h>
#include <string>

QueueHandle_t usart_tx_q = nullptr;

void usart_setup(void)
{
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_USART1);
  // USART TX is PA9
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);

  usart_set_baudrate(USART1, 115200);
  usart_set_databits(USART1, 8);
  usart_set_stopbits(USART1, USART_STOPBITS_1);
  usart_set_mode(USART1, USART_MODE_TX);
  usart_set_parity(USART1, USART_PARITY_NONE);
  usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
  usart_enable(USART1);

  usart_tx_q = xQueueCreate(2048, sizeof(char));
}

// Warning: std::to_string does not work on uint64_t types!
void usart_puts(std::string s, bool no_endline = false)
{
  for (std::size_t i(0); i < s.size(); ++i)
    xQueueSendToBack(usart_tx_q, &s[i], portMAX_DELAY);

  if (no_endline)
    return;

  char a = '\r';
  char b = '\n';
  xQueueSendToBack(usart_tx_q, &a, portMAX_DELAY);
  xQueueSendToBack(usart_tx_q, &b, portMAX_DELAY);
}

void task_usart_tx(void *)
{
  char c;
  while (1)
  {
    if (xQueueReceive(usart_tx_q, &c, portMAX_DELAY) == pdPASS)
    {
      while (!usart_get_flag(USART1, USART_SR_TXE))
        taskYIELD();
      usart_send(USART1, c);
    }
  }
}

#endif
