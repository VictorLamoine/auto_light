# Dependencies
```bash
sudo apt install -y cmake python
```

You need [texane/stlink](https://github.com/texane/stlink/) installed if you want to upload via a ST-Link V2.

# Building
```bash
mkdir -p auto_light/build
cd auto_light
git clone --recurse-submodules https://gitlab.com/VictorLamoine/auto_light.git src
cd build
cmake ../src
make -j4
```

To upload, make sure the Blue-Pill / ST-Link are connected and use one of the two commands:
```bash
make -j2 auto_light.bin_upload
```

# Mechanical design
- [Casing - Bot.step](cad/Casing - Bot.step)
- https://cad.onshape.com/documents/af7bb18fd16a5daadf3f1849/w/524d00ccd6f88729b5cd6775/e/7c6dbb84dde7cd2922d48409
